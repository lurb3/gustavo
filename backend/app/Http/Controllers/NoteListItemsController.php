<?php

namespace App\Http\Controllers;

use App\Models\NoteListItem;
use Illuminate\Http\Request;

class NoteListItemsController extends Controller
{
    public function show()
    {
        $userId = auth()->user()->id;

        try {
            $noteLists = NoteListItem::where('user_id', $userId)->orderBy('position')->get();
        } catch (\Exception $e) {
            return $e;
        }

        return response()->json([
            'note_lists' => $noteLists
        ]);
    }
}
