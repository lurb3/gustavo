<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExpensesRequest;
use App\Models\Category;
use App\Models\Expense;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ExpensesController extends Controller
{
    public function show(Request $request): JsonResponse
    {
        $userId = auth()->user()->id;
        $currentPage = $request->current_page;
        $filters = $request->only(['show_archived']);
        $dateFrom = data_get($request, 'date_from', now()->startOfDay());
        $dateTo = data_get($request, 'date_to', now()->endOfDay());
        $perPage = 10;

        $expenses = Expense::where('user_id', $userId)
            ->with('category')
            ->orderBy('transaction_date', 'desc')
            ->where('is_archived', 0)
            ->whereBetween('transaction_date', [$dateFrom, $dateTo])
            ->when(!empty($filters['show_archived']), function ($query) {
                $query->orWhere('is_archived', 1);
            });

        $totalAmount = $expenses->sum('amount');
        $total = $expenses->count();

        $expenses = $expenses
            ->skip(($currentPage - 1) * $perPage)
            ->take($perPage)
            ->get();

        return response()->json([
            'totalAmount' => $totalAmount,
            'expenses' => $expenses,
            'total' => $total,
            'current_page' => $currentPage
        ]);
    }

    public function getArchivedExpenses(Request $request): JsonResponse
    {
        $userId = auth()->user()->id;
        $currentPage = $request->current_page;
        $dateFrom = data_get($request, 'date_from', now()->startOfDay());
        $dateTo = data_get($request, 'date_to', now()->endOfDay());
        $perPage = 10;

        $expenses = Expense::where('user_id', $userId)
            ->orderBy('transaction_date', 'desc')
            ->where('is_archived', 1)
            ->whereBetween('transaction_date', [$dateFrom, $dateTo]);

        $total = $expenses->count();

        $expenses = $expenses
            ->skip(($currentPage - 1) * $perPage)
            ->take($perPage)
            ->get();

        return response()->json([
            'expenses' => $expenses,
            'total' => $total,
            'current_page' => $currentPage
        ]);
    }

    public function create(ExpensesRequest $request): JsonResponse
    {
        $user = auth()->user();
        $validated = $request->validated();

        try {
            $expense = Expense::create([
                'user_id' => $user->id,
                'name' => $validated['name'],
                'amount' => $validated['amount'],
                'transaction_date' => $validated['transaction_date'] ?? date('Y-m-d H:i:s'),
            ]);

            if ($validated['category']) {
                $expense = $expense->fresh();

                $category = Category::firstOrNew(
                    [
                        'user_id' => $user->id,
                        'name' => $validated['category'],
                    ],
                );

                $category->save();
                $expense->update(['category_id' => $category->id]);
            }

            return response()->json(['expense' => $expense]);
        } catch (\Exception $e) {
            Log::error('Could not create expense', [
                'user_id' => $user->id,
                'transaction_date' => $validated['transaction_date'] ?? date('Y-m-d H:i:s'),
                'message' => $e->getMessage(),
            ]);

            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function importCSV(Request $request)
    {
        $validated = $request->validate([
            'csv' => [
                'array',
                'each' => [
                    'required',
                    'array',
                    'keys' => [
                        'name' => 'required',
                        'category' => 'string',
                        'amount' => 'required',
                        'created_at' => 'required',
                    ],
                ],
            ],
        ]);
        // TODO: Check if category is on csv, if it is search id from categories and insert category id in expense
        try {
            foreach($validated['csv'] as $csv) {
                if (!$csv['name'] || !$csv['amount'] || !$csv['created_at']) {
                    continue;
                }
                //TODO: CHECK IF USER HAS CATEGORY, IF NOT ADD CATEGORY
                Expense::create([
                    'name' => $csv['name'],
                    'amount' => $csv['amount'],
                    'category' => data_get($csv, 'category', ''),
                    'transaction_date' => Carbon::parse($csv['created_at']),
                    'user_id' => auth()->user()->id
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['message' => 'Csv imported successfully']);
    }

    // TODO: Move update, restore, archive to ExpenseService (too much duplicated code)
    public function update(ExpensesRequest $request, Expense $expense): JsonResponse
    {
        $userId = auth()->user()->id;
        $validated = $request->validated();

        if ($expense->user_id !== $userId) {
            return response()->json(['message' => 'Cannot update this expense'], 403);
        }

        if ($expense->is_archived) {
            return response()->json(['message' => 'Expense is archived'], 422);
        }

        try {
            $expense->update($validated);
        } catch (\Exception $e) {
            Log::error('Could not update expense', [
                'id' => $validated->id,
                'user_id' => $userId,
                'transaction_date' => $validated->transaction_date,
                'created_at' => $validated->created_at,
                'message' => $e->getMessage(),
            ]);

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['expense' => $expense]);
    }

    public function restore(Expense $expense): JsonResponse
    {
        $userId = auth()->user()->id;

        if ($expense->user_id !== $userId) {
            return response()->json(['message' => 'Cannot restore this expense'], 403);
        }

        if (!$expense->is_archived) {
            return response()->json(['message' => 'Expense is already active'], 422);
        }

        try {
            $expense->update(['is_archived' => 0]);
        } catch (\Exception $e) {
            Log::error('Could not restore expense', [
                'id' => $expense->id,
                'user_id' => $userId,
                'transaction_date' => $expense->transaction_date,
                'created_at' => $expense->created_at,
                'message' => $e->getMessage(),
            ]);

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['expense' => $expense]);
    }

    public function archive(Expense $expense): JsonResponse
    {
        $userId = auth()->user()->id;

        if ($expense->user_id !== $userId) {
            return response()->json(['message' => 'Cannot archive this expense'], 403);
        }

        if ($expense->is_archived) {
            return response()->json(['message' => 'Expense is already archived'], 422);
        }

        try {
            $expense->update(['is_archived' => 1]);
        } catch (\Exception $e) {
            Log::error('Could not archive expense', [
                'id' => $expense->id,
                'user_id' => $userId,
                'transaction_date' => $expense->transaction_date,
                'created_at' => $expense->created_at,
                'message' => $e->getMessage(),
            ]);

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['expense' => $expense]);
    }

    public function delete(Expense $expense): JsonResponse
    {
        $userId = auth()->user()->id;

        if ($expense->user_id !== $userId) {
            return response()->json(['message' => 'Cannot delete this expense'], 403);
        }

        try {
            $expense->delete();
        } catch (\Exception $e) {
            Log::error('Could not delete expense', [
                'id' => $expense->id,
                'user_id' => $userId,
                'transaction_date' => $expense->transaction_date,
                'created_at' => $expense->created_at,
                'message' => $e->getMessage(),
            ]);

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json();
    }
}
