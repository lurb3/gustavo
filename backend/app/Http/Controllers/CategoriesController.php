<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function show(Request $request): JsonResponse
    {
        $userId = auth()->user()->id;

        $categories = Category::where('user_id', $userId)
            ->get();

        return response()->json([
            'categories' => $categories,
        ]);
    }

    public function create(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'string|required'
        ]);

        $userId = auth()->user()->id;

        $category = Category::where('user_id', $userId)
            ->where('name', $request->name)
            ->exists();

        if ($category) {
            return response()->json(['message' => 'Category already exists'], 400);
        }

        Category::create([
            'name' => $request->name,
            'user_id' => $userId
        ]);

        return response()->json();
    }
}
