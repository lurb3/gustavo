<?php

namespace App\Http\Controllers;

use App\Models\NoteList;
use Illuminate\Http\Request;

class NoteListsController extends Controller
{
    public function show()
    {
        $noteLists = NoteList::where('user_id', auth()->user()->id)
            ->orderBy('title')
            ->get();

        return response()->json([
            'lists' => $noteLists
        ]);
    }

    public function create(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:3|max:255',
        ]);

        $validated['user_id'] = auth()->user()->id;

        NoteList::create($validated);
    }
}
