<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class ExpensesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): Authenticatable
    {
        return auth()->user();
    }

    public function rules(): array
    {
        $rules = [
            'name' => 'string',
            'amount' => 'integer|max:999999|min:-999999',
            'currency' => 'nullable|string',
            'category' => 'nullable|string',
            'transaction_date' => 'nullable|date',
        ];

        if ($this->isMethod('post')) {
            $rules['name'] .= '|required';
            $rules['amount'] .= '|required';
        }

        return $rules;
    }

    protected function failedValidation(Validator $validator): HttpResponseException
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
