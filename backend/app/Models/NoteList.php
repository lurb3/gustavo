<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class NoteList extends Model
{
    use HasFactory;

    protected $table = 'note_lists';

    protected $fillable = [
        'title',
        'user_id'
    ];

    public function noteListItem(): HasMany
    {
        return $this->hasMany(NoteListItem::class);
    }
}
