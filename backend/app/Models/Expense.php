<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Expense extends Model
{
    use HasFactory;

    protected $table = 'expenses';

    protected $fillable = [
        'name',
        'amount',
        'category_id',
        'currency',
        'transaction_date',
        'user_id',
        'is_archived',
    ];

    protected $casts = [
        'is_archived' => 'boolean',
    ];

    public function getIsArchivedAttribute()
    {
        return $this->attributes['is_archived'] ?? false;
    }

    public function category(): belongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }
}
