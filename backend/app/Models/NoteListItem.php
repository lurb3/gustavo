<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class NoteListItem extends Model
{
    use HasFactory;

    protected $table = 'note_list_items';

    protected $fillable = [
        'title',
        'description',
        'note_list_id',
        'user_id'
    ];

    public function notes(): HasOne
    {
        return $this->hasOne(NoteList::class);
    }
}
