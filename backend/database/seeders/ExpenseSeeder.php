<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Expense;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersId = User::take(20)
            ->get()
            ->pluck('id')
            ->toArray();

        $adminUser = User::where('name', 'admin')
            ->first();

        $users = array_merge($usersId, [$adminUser->id]);

        foreach($users as $id) {
            $category = Category::factory()
                ->withUserId($id)
                ->create();

            Expense::factory()
                ->withUserId($id)
                ->withCategoryId($category->id)
                ->count(30)
                ->create();
        }
    }
}
