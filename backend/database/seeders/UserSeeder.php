<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserRoles;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(50)
            ->create();

        $adminUser = User::create([
            'name' => 'admin',
            'email' => 'gustavo.gigante.s@gmail.com',
            'password' => '123'
        ]);

        UserRoles::create([
            'user_id' => $adminUser->id,
            'role_id' => 1,
        ]);
    }
}
