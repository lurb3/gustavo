<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Expense>
 */
class ExpenseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->word(),
            'amount' => fake()->randomNumber(3),
            'category_id' => null,
            'transaction_date' => fake()->dateTimeThisMonth(),
            'user_id' => null,
        ];
    }

    public function withUserId($userId)
    {
        return $this->state(function () use ($userId) {
            return [
                'user_id' => $userId,
            ];
        });
    }
    public function withCategoryId($categoryId)
    {
        return $this->state(function () use ($categoryId) {
            return [
                'category_id' => $categoryId,
            ];
        });
    }
}
