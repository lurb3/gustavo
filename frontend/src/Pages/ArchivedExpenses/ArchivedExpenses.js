import React, { useEffect, useState } from 'react';
import {Table, Button, notification, Pagination, Popconfirm, DatePicker} from "antd";
import api from "../../Services/Auth/api";
import css from './ArchivedExpenses.module.scss';
import { Typography, Checkbox } from 'antd';
import dayjs from "dayjs";

const Expenses = () => {
    const [ expenses, setExpenses ] = useState([]);
    const [ currentPage, setCurrentPage ] = useState(1);
    const [ total, setTotal ] = useState(0);
    const [ filterDate, setFilterDate ] = useState([
        dayjs().add(-7, 'day'),
        dayjs(),
    ]);

    const { RangePicker } = DatePicker;
    const rangePresets = [
        {
            label: 'Last 7 Days',
            value: [dayjs().add(-7, 'd'), dayjs()],
        },
        {
            label: 'Last 14 Days',
            value: [dayjs().add(-14, 'd'), dayjs()],
        },
        {
            label: 'Last 30 Days',
            value: [dayjs().add(-30, 'd'), dayjs()],
        },
        {
            label: 'Last 90 Days',
            value: [dayjs().add(-90, 'd'), dayjs()],
        },
    ];

    const { Text } = Typography;

    const handleDelete = async (expenseId) => {
        if (!expenseId) return;
        await api.delete(`/api/expenses/${expenseId}`);
        getData();
    };

    const columns = [
        {title: 'Name', dataIndex: 'name', key: 'name'},
        {title: 'Amount', dataIndex: 'amount', key: 'amount'},
        {title: 'Currency', dataIndex: 'currency', key: 'currency'},
        {title: 'Date', dataIndex: 'date', key: 'date'},
        {title: 'Status', dataIndex: 'status', key: 'status'},
        {
            title: 'Actions',
            dataIndex: 'actions',
            render: (_, record) =>
                expenses.length >= 1 ? (
                    <>
                        <Popconfirm title="Are you sure?" onConfirm={() => handleDelete(record.id)}>
                            <a>Delete</a>
                        </Popconfirm>
                    </>
                ) : null,
        },
    ]
    const getData = async () => {
        const result = await api.get('/api/expenses/archived', {
            params: {
                current_page: currentPage,
                date_from: filterDate[0].format('YYYY/MM/DD'),
                date_to: filterDate[1].format('YYYY/MM/DD'),
            }
        });

        if (result?.data?.expenses) {
            const data = result.data.expenses.map((expense, index) => {
                return {
                    key: index,
                    id: expense.id,
                    is_archived: expense.is_archived,
                    name: expense.name,
                    amount: expense.amount,
                    currency: expense.currency,
                    date: expense.transaction_date,
                    status: expense.is_archived ? <Text type="danger">Archived</Text> : <Text type="success">Active</Text>
                }
            })
            setTotal(result.data.total);
            setExpenses(data);
        }
    }

    const handleDateChange = (date) => {
        if (!date) {
            setFilterDate([]);
            return;
        }

        setFilterDate([
            dayjs(date[0]),
            dayjs(date[1]),
        ]);
    }

    useEffect(() => {
        getData();
    }, [currentPage, filterDate])

    return (
        <div>
            <div className={css.header}>
                <Typography.Title level={1}>
                    Archived Expenses
                </Typography.Title>
            </div>
            <div style={{ marginBottom: '20px' }}>
                <Typography.Title level={4}>
                    Filters
                </Typography.Title>
                <div className={css.filtersWrapper}>
                    <div>Date <RangePicker defaultValue={filterDate} presets={rangePresets} onChange={(date) => handleDateChange(date)} /></div>
                </div>
            </div>
            <Table dataSource={expenses} columns={columns} pagination={false} style={{ marginTop: '10px', marginBottom: '10px'}}/>
            <Pagination total={total} current={currentPage} onChange={setCurrentPage}/>
        </div>
    )
}

export default Expenses;