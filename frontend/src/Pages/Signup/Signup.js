import React, {useState} from 'react';
import {Link, redirect} from "react-router-dom";
import api from "Services/Auth/api";
import {Button, Form, Input, notification} from 'antd';
import css from "../Login/Login.module.scss";

const Signup = () => {
    const [context, contextHolder] = notification.useNotification();
    const [isLoading, setIsLoading] = useState(false);

    const onSubmit = async (data) => {
        setIsLoading(true);

        const result = await api.post('/api/signup', data);

        setIsLoading(false);

        if (result.status !== 200) {
            context.error({
                message: 'Error',
                description:
                    result?.response?.data?.message || "Account not created",
                duration: 2
            });
            return;
        }
        context.success({
            message: 'Success',
            description:
                'Account created',
            duration: 2
        });
    }

    return (
        <>
            {contextHolder}
            <h2>Create a new account</h2>
            <Form
                name="basic"
                style={{maxWidth: 400}}
                initialValues={{remember: true}}
                onFinish={onSubmit}
                autoComplete="off"
                className={css.formWrapper}
            >
                <div>Username</div>
                <Form.Item
                    name="name"
                    rules={[{required: true, message: 'Please input your username!',}]}
                >
                    <Input />
                </Form.Item>

                <div>Email</div>
                <Form.Item
                    name="email"
                    rules={[{required: true, message: 'Please input your email!',}]}
                >
                    <Input />
                </Form.Item>

                <div>Password</div>
                <Form.Item
                    name="password"
                    rules={[{required: true, message: 'Please input your password!',}]}
                >
                    <Input.Password />
                </Form.Item>
                <Link to={"/"}>Already have an account?</Link>


                <Form.Item
                >
                    <Button type="primary" htmlType="submit" disabled={isLoading}>
                        Create account
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default Signup;