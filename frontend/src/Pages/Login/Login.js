import {useEffect, useState} from 'react';
import { useForm } from "react-hook-form";
import {Link, useNavigate} from "react-router-dom";
import { Button, Form, Input, notification } from 'antd';
import { setUser } from "Services/Auth/slice";
import api from "Services/Auth/api";
import css from "./Login.module.scss";
import { useDispatch } from "react-redux";
import {isAuthenticated, setToken} from "Services/Auth/auth";
const Login = () => {
    const {
        formState: { errors },
    } = useForm();
    const authenticated = isAuthenticated();
    const navigate = useNavigate();
    const [context, contextHolder] = notification.useNotification();
    const dispatch = useDispatch();
    const [ isLoading, setIsLoading ] = useState(false);

    const onSubmit = async (data) => {
        setIsLoading(true);

        await api.get('/sanctum/csrf-cookie');
        const auth = await api.post('/api/login', data);

        setIsLoading(false);

        if (! auth?.data?.token) {
            context.error({
                message: 'Error',
                description:
                    auth?.response?.data?.message || "User not logged in",
                duration: 2
            });
            return;
        }
        context.success({
            message: 'Success',
            description:
                'Logged in Successfully',
            duration: 2,
            onClose: () => navigate('/expenses')
        });

        dispatch(setUser(auth.data.user));
        setToken(auth.data.token);
    }

    useEffect(() => {
        if (authenticated) {
            navigate('/expenses');
        }
    }, [])

    return (
        <>
        {contextHolder}
            <h2>Log in</h2>
            <Form
                name="basic"
                style={{maxWidth: 400}}
                initialValues={{remember: true}}
                onFinish={onSubmit}
                autoComplete="off"
                className={css.formWrapper}
            >
                <div>Email</div>
                <Form.Item
                    name="email"
                    rules={[{required: true, message: 'Please input your email!',}]}
                >
                    <Input />
                </Form.Item>

                <div>Password</div>
                <Form.Item
                    name="password"
                    rules={[{required: true, message: 'Please input your password!',}]}
                >
                    <Input.Password />
                </Form.Item>
                <Link to={"/signup"}>Create a new account</Link>

                <Form.Item
                >
                    <Button type="primary" htmlType="submit" disabled={isLoading}>
                        Log in
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default Login;