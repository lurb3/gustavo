import React, { useEffect, useState } from 'react';
import {Table, Button, notification, Pagination, Popconfirm, DatePicker} from "antd";
import { ImportOutlined, PlusOutlined } from '@ant-design/icons';
import api from "../../Services/Auth/api";
import css from './Expenses.module.scss';
import ExpensesForm from "./ExpensesForm";
import { Typography, Checkbox } from 'antd';
import dayjs from "dayjs";
import CSVReader from "react-csv-reader";
import HorizontalChart from "Components/Charts/HorizontalChart";

const Expenses = () => {
    const [context, contextHolder] = notification.useNotification();
    const [ expenses, setExpenses ] = useState([]);
    const [ openFormModal, setOpenFormModal ] = useState(false);
    const [ currentPage, setCurrentPage ] = useState(1);
    const [ total, setTotal ] = useState(0);
    const [ totalAmount, setTotalAmount ] = useState(0);
    const [ showArchived, setShowArchived ] = useState(false);
    const [ filterDate, setFilterDate ] = useState([
        dayjs().subtract(7, 'day'),
        dayjs(),
    ]);

    const { RangePicker } = DatePicker;
    const rangePresets = [
        {
            label: 'Last 7 Days',
            value: [dayjs().add(-7, 'd'), dayjs()],
        },
        {
            label: 'Last 14 Days',
            value: [dayjs().add(-14, 'd'), dayjs()],
        },
        {
            label: 'Last 30 Days',
            value: [dayjs().add(-30, 'd'), dayjs()],
        },
        {
            label: 'Last 90 Days',
            value: [dayjs().add(-90, 'd'), dayjs()],
        },
    ];

    const { Text } = Typography;

    const handleArchive = async (expenseId) => {
        if (!expenseId) return;
        await api.patch(`/api/expenses/archive/${expenseId}`);
        getData();
    };

    const handleDelete = async (expenseId) => {
        if (!expenseId) return;
        await api.delete(`/api/expenses/${expenseId}`);
        getData();
    };

    const columns = [
        {title: 'Name', dataIndex: 'name', key: 'name'},
        {title: 'Category', dataIndex: 'category', key: 'category'},
        {title: 'Amount', dataIndex: 'amount', key: 'amount'},
        {title: 'Currency', dataIndex: 'currency', key: 'currency'},
        {title: 'Date', dataIndex: 'date', key: 'date'},
        {title: 'Status', dataIndex: 'status', key: 'status'},
        {
            title: 'Actions',
            dataIndex: 'actions',
            render: (_, record) =>
                expenses.length >= 1 ? (
                    <>
                        <Popconfirm title="Are you sure?" onConfirm={() => handleArchive(record.id)}>
                            <a style={{ marginRight: '10px' }}>Archive</a>
                        </Popconfirm>
                        <Popconfirm title="Are you sure?" onConfirm={() => handleDelete(record.id)}>
                            <a>Delete</a>
                        </Popconfirm>
                    </>
                ) : null,
        },
    ]
    const getData = async () => {
        const result = await api.get('/api/expenses', {
            params: {
                current_page: currentPage,
                show_archived: showArchived ? 1 : 0,
                date_from: filterDate[0].format('YYYY/MM/DD'),
                date_to: filterDate[1].format('YYYY/MM/DD'),
            }
        });

        if (result?.data?.expenses) {
            const data = result.data.expenses.map((expense, index) => {
                return {
                    key: index,
                    id: expense.id,
                    is_archived: expense.is_archived,
                    name: expense.name,
                    category: expense?.category?.name,
                    amount: expense.amount,
                    currency: expense.currency,
                    date: expense.transaction_date,
                    status: expense.is_archived ? <Text type="danger">Archived</Text> : <Text type="success">Active</Text>
                }
            })
            setTotalAmount(result.data.totalAmount);
            setTotal(result.data.total);
            setExpenses(data);
        }
    }

    const handleExpenseSuccess = () => {
        context.success({
            message: 'Success',
            description: 'Expense created',
            duration: 2,
        });
        getData();
    }

    const handleDateChange = (date) => {
        if (!date) {
            setFilterDate([]);
            return;
        }

        setFilterDate([
            dayjs(date[0]),
            dayjs(date[1]),
        ]);
    }

    const handleImportCsv = async (data) => {
        const result = await api.post('/api/expenses/import-csv', {
            csv: data
        });

        if (result.status === 200) {
            context.success({
                message: 'Success',
                description: 'CSV imported successfully',
                duration: 2,
            });
        } else {
            context.error({
                message: 'Error',
                description: result?.response?.data?.message || "CSV was not imported",
                duration: 2
            });
        }
    }

    useEffect(() => {
        getData();
    }, [currentPage, showArchived, filterDate])

    return (
        <div>
            {contextHolder}
            <div className={css.header}>
                <Typography.Title level={1}>
                    Expenses
                </Typography.Title>
                <div>
                    <CSVReader
                        parserOptions={{ header: true }}
                        onFileLoaded={(data) => handleImportCsv(data)}
                    />
                    <Button type="primary" onClick={() => setOpenFormModal(true)}><PlusOutlined /> Add expense</Button>
                </div>
            </div>
            <div style={{width:'50%'}}>
                {/*<HorizontalChart />*/}
            </div>
            <div style={{ marginBottom: '20px' }}>
                <Typography.Title level={4}>
                    Filters
                </Typography.Title>
                <div className={css.filtersWrapper}>
                    <div>Archived <Checkbox checked={showArchived} onChange={(e) => setShowArchived(e.target.checked)} /></div>
                    <div>Date <RangePicker defaultValue={filterDate} presets={rangePresets} onChange={(date) => handleDateChange(date)} /></div>
                </div>
            </div>
            <Typography.Title level={4} style={{ margin: 0 }}>
                Total expenses: {totalAmount}
            </Typography.Title>
            <Table dataSource={expenses} columns={columns} pagination={false} style={{ marginTop: '10px', marginBottom: '10px'}}/>
            <Pagination total={total} current={currentPage} onChange={setCurrentPage}/>
            { openFormModal && <ExpensesForm open={openFormModal} setOpen={setOpenFormModal} callback={handleExpenseSuccess} /> }
        </div>
    )
}

export default Expenses;