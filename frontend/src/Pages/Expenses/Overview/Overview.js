import React from 'react';
import css from "./Overview.module.scss";
import {Typography} from "antd";

const Overview = () => {
    return (
        <div>
            <div className={css.header}>
                <Typography.Title level={1}>
                    Overview
                </Typography.Title>
            </div>
        </div>
    )
}

export default Overview;