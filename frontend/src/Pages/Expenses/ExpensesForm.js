import React, {useEffect, useRef, useState} from 'react';
import {Modal, Form, Input, notification, DatePicker, Button, Select, Divider, Space} from "antd";
import css from "./ExpensesForm.module.scss";
import dayjs from "dayjs";
import api from "../../Services/Auth/api";
import {PlusOutlined} from "@ant-design/icons";

const ExpensesForm = ({ open, setOpen, callback }) => {
    const { Option } = Select;
    const [ form ] = Form.useForm();
    const [context, contextHolder] = notification.useNotification();
    const [ userCategories, setUserCategories ] = useState([]);
    const [ newCategory, setNewCategory ] = useState(null);
    const [ isLoading, setIsLoading ] = useState(false);

    const closeForm = () => {
        form.resetFields();
        callback();
        setOpen(false);
    }

    const onSubmit = async (data) => {
        data.transaction_date = data.transaction_date ? dayjs(data.transaction_date).format('YYYY/MM/DD') : null;
        setIsLoading(true);
        const result = await api.post('/api/expenses', data);

        if (! result?.data?.expense) {
            context.error({
                message: 'Error',
                description: result?.response?.data?.message || "Expense not created",
                duration: 2
            });
        }
        setIsLoading(false);
        closeForm();
    }

    // TODO: Move categories fetch to redux (?) and load categories on user login or something (?)
    const getUserCategories = async () => {
        setIsLoading(true);
        const result = await api.get('/api/categories');
        setIsLoading(false);

        setUserCategories(result?.data?.categories || []);
    }

    const onNewCategoryChange = (event) => {
        setNewCategory(event.target.value)
    };

    const addCategory = async () => {
        setIsLoading(true);
        const res = await api.post('/api/categories', { name: newCategory });
        setIsLoading(false);

        if (res?.response && res?.response?.status !== 200) {
            context.error({
                message: 'Error',
                description: res.response?.data?.message || "Category not created",
                duration: 2
            });

            return;
        }

        context.success({
            message: 'Success',
            description: "New category created",
            duration: 2
        });

        getUserCategories();
    }

    useEffect(() => {
        getUserCategories();
    }, []);

    return (
        <>
            {contextHolder}
            <Modal
                title="Add new expense"
                open={open}
                confirmLoading={isLoading}
                footer={null}
                onCancel={() => setOpen(false)}
            >
                <Form
                    form={form}
                    name="basic"
                    style={{maxWidth: 400}}
                    initialValues={{remember: true}}
                    onFinish={onSubmit}
                    autoComplete="off"
                    className={css.formWrapper}
                >
                    <div>Name</div>
                    <Form.Item
                        name="name"
                        rules={[{required: true, message: 'Name is required',}]}
                    >
                        <Input />
                    </Form.Item>

                    <div>Amount</div>
                    <Form.Item
                        name="amount"
                        rules={[{required: true, message: 'Amount is required',}]}
                    >
                        <Input type={"number"} />
                    </Form.Item>

                    <div>Category</div>
                    <Form.Item
                        name="category"
                    >
                        <Select
                            placeholder="Select a option and change input text above"
                            disabled={isLoading}
                            allowClear
                            dropdownRender={(menu) => (
                                <>
                                    {menu}
                                    <Divider style={{ margin: '8px 0' }} />
                                    <Space style={{ padding: '0 8px 4px' }}>
                                        <Input
                                            placeholder="Please enter item"
                                            value={newCategory}
                                            onChange={onNewCategoryChange}
                                            onKeyDown={(e) => e.stopPropagation()}
                                        />
                                        <Button type="text" icon={<PlusOutlined />} onClick={addCategory}>
                                            Add item
                                        </Button>
                                    </Space>
                                </>
                            )}
                        >
                            {
                                userCategories.map((category) =>
                                    <Option value={category.name} key={category.name}>
                                        {category.name}
                                    </Option>
                                )
                            }
                        </Select>
                    </Form.Item>

                    <div>Transaction date</div>
                    <Form.Item
                        name="transaction_date"
                    >
                        <DatePicker format="DD/MM/YYYY" style={{ width: '100%' }} />
                    </Form.Item>

                    <Form.Item
                    >
                        <Button type="primary" htmlType="submit" disabled={isLoading}>
                            Create
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    )
}

export default ExpensesForm;