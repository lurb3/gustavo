import api from "./api";
import {removeToken} from "./auth";
const useAuth = () => {
    const logout = async () => {
        await api.post('/api/logout');
        removeToken();
    }

    return {
        logout: logout
    };
}

export default useAuth;