import { createSlice } from '@reduxjs/toolkit'

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: {},
    },
    reducers: {
        setUser: (state, action) => {
            state.user = action.payload
        },
    },
})

export const authSelector = {
    getUser: (state) => state?.user?.user,
};
export const { setUser } = authSlice.actions;
export default authSlice.reducer;