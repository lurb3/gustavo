import axios from "axios";
import {getToken, removeToken} from "./auth";

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    timeout: 8000,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
    },
    withCredentials: true
})

api.interceptors.request.use((config) => {
    const token = getToken();
    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
});

api.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error?.response?.status === 401) {
            removeToken();
            window.location.href = '/';
        }
        return error;
    }
);

export default api;