const isAuthenticated = () => {
    return !!localStorage.getItem('authToken');
}

const removeToken = () => {
    localStorage.removeItem('authToken');
}

const getToken = () => {
    return localStorage.getItem('authToken');
}

const setToken = (v) => {
    localStorage.setItem('authToken', v);
}

export {
    isAuthenticated,
    removeToken,
    getToken,
    setToken,
}