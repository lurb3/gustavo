import React, { useState, useEffect } from 'react';
import {
    EyeOutlined,
    DollarOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    SettingOutlined,
    LoginOutlined,
    UserOutlined,
    FolderOutlined
} from '@ant-design/icons';
import { Layout, Menu, Button, theme } from 'antd';
import Swal from "sweetalert2";
import { isAuthenticated } from "Services/Auth/auth";
import useAuth from "Services/Auth/useAuth";
import { useNavigate } from "react-router-dom";
const { Header, Sider, Content } = Layout;

const Navbar = ({children}) => {
    const { logout } = useAuth();
    const authenticated = isAuthenticated();
    const [collapsed, setCollapsed] = useState(false);
    const [items, setItems] = useState([]);
    const navigate = useNavigate();
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    const handleLogout = () => {
        Swal.fire({
            title: "Logout",
            text: "Are you sure?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "red",
            cancelButtonColor: "#1677ff",
            confirmButtonText: "Logout"
        }).then(async (result) => {
            if (result.isConfirmed) {
                await logout();
                navigate('/');
            }
        });
    }

    useEffect(() => {
        if (! authenticated) {
            setItems([
                {
                    key: '1',
                    icon: <LoginOutlined />,
                    label: 'Login',
                },
            ]);
        } else {
            setItems([
                {
                    key:'8',
                    icon: <UserOutlined />,
                    label: 'Profile',
                    onClick: () => {},
                },
                {
                    key: '2',
                    icon: <DollarOutlined />,
                    label: 'Expenses',
                    children: [
                        {
                            key:'6',
                            icon: <EyeOutlined />,
                            label: 'Overview',
                            onClick: () => navigate('/expenses/overview'),
                        },
                        {
                            key: '3',
                            icon: <DollarOutlined />,
                            label: 'Expenses',
                            onClick: () => navigate('/expenses')
                        },
                        {
                            key: '4',
                            icon: <FolderOutlined />,
                            label: 'Archived',
                            onClick: () => navigate('/expenses/archived')
                        },
                    ]
                },
                {
                    key:'7',
                    icon: <SettingOutlined />,
                    label: 'Settings',
                    onClick: () => {},
                },
                {
                    key: '5',
                    icon: <LoginOutlined />,
                    label: 'Logout',
                    onClick: handleLogout
                },
            ]);
        }
    }, [authenticated])

    return (
        <Layout style={{minHeight: '100vh'}}>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="demo-logo-vertical" />
                <Menu
                    theme="dark"
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    items={items}
                />
            </Sider>
            <Layout>
                <Header
                    style={{
                        padding: 0,
                        background: colorBgContainer,
                    }}
                >
                    <Button
                        type="text"
                        icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                        onClick={() => setCollapsed(!collapsed)}
                        style={{
                            fontSize: '16px',
                            width: 64,
                            height: 64,
                        }}
                    />
                </Header>
                <Content
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                        background: colorBgContainer,
                    }}
                >
                    {children}
                </Content>
            </Layout>
        </Layout>
    );
};
export default Navbar;