import {useEffect, useState} from 'react'
import {Navigate, useLocation} from "react-router-dom"
import {isAuthenticated} from "../Services/Auth/auth";

const ProtectedRoute = ({children}) => {
    const authenticated = isAuthenticated();
    const location = useLocation();

    if (!authenticated) {
        return <Navigate to="/" state={{ from: location}} replace />
    }
    return children;
};

export default ProtectedRoute;