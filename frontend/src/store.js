import { configureStore } from '@reduxjs/toolkit'
import authSlice from "Services/Auth/slice";

export default configureStore({
    reducer: {
        user: authSlice,
    },
})