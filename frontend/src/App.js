import './App.css';
import {BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store";
import {ConfigProvider} from "antd";
import ProtectedRoute from "routes/ProtectedRoute";
import Login from "Pages/Login/Login";
import Signup from "Pages/Signup/Signup";
import NotFound from "Pages/NotFound/NotFound";
import Expenses from "Pages/Expenses/Expenses";
import Navbar from "Components/Navbar/Navbar";
import ArchivedExpenses from "./Pages/ArchivedExpenses/ArchivedExpenses";
import Overview from "./Pages/Expenses/Overview/Overview";

function App() {
    return (
      <Provider store={store}>
          <ConfigProvider>
              <BrowserRouter>
                  <Navbar>
                      <Routes>
                          <Route path="/expenses">
                              <Route index element={
                                  <ProtectedRoute>
                                      <Expenses />
                                  </ProtectedRoute>
                                }
                              />
                              <Route path="overview" element={
                                  <ProtectedRoute>
                                      <Overview />
                                  </ProtectedRoute>
                                }
                              />
                              <Route path="archived" element={
                                  <ProtectedRoute>
                                      <ArchivedExpenses />
                                  </ProtectedRoute>
                              }
                              />
                          </Route>
                          <Route path="/signup" element={<Signup />} />
                          <Route
                              path="/" element={<Login />}
                          />
                          <Route path="*" element={<NotFound />} />
                      </Routes>
                  </Navbar>
              </BrowserRouter>
          </ConfigProvider>
      </Provider>
    );
}

export default App;
